from django.db import models


class CrimeRecords(models.Model):
    country = models.CharField(max_length=256)
    year = models.SmallIntegerField()
    count = models.SmallIntegerField()
    rate = models.FloatField()
    source = models.CharField(max_length=256)
    source_type = models.CharField(max_length=256)
