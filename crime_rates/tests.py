from django.contrib.auth.models import User
from django.test import TestCase

from rest_framework.test import APIClient

from crime_rates.models import CrimeRecords

client = APIClient()


class CrimeRatesTestCase(TestCase):

    def setUp(self):
        user = User.objects.create(username='timbest', password='abc123')
        client.force_authenticate(user=user)
        record_a = CrimeRecords.objects.create(
            country="USA",
            year=2015,
            count=2897,
            rate=4.5,
            source="CTS",
            source_type="CJ"
        )
        record_b = CrimeRecords.objects.create(
            country="France",
            year=2015,
            count=3,
            rate=0.5,
            source="CTS",
            source_type="CJ"
        )

    def test_get_record(self):
        response = client.get('/crime-records/', {'country': 'France', 'year':2015}, format='json')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.content,
            '{"count":1,"next":null,"previous":null,"results":[{"id":2,"country":"France","year":2015,"count":3,"rate":0.5,"source":"CTS","source_type":"CJ"}]}'
        )

    def test_get_record_list(self):
        response = client.get('/crime-records/', format='json')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.content,
            '{"count":2,"next":null,"previous":null,"results":[{"id":1,"country":"USA","year":2015,"count":2897,"rate":4.5,"source":"CTS","source_type":"CJ"},{"id":2,"country":"France","year":2015,"count":3,"rate":0.5,"source":"CTS","source_type":"CJ"}]}'
        )

    def test_create_record(self):
        response = client.post(
            '/crime-records/',
            {"country":"USA","year":2015,"count":2897,"rate":4.5,"source":"CTS","source_type":"CJ"},
            format='json'
        )

        self.assertEqual(response.status_code, 201)
        self.assertEqual(
            response.content,
            '{"id":3,"country":"USA","year":2015,"count":2897,"rate":4.5,"source":"CTS","source_type":"CJ"}'
        )

    def test_update_record(self):
        response = client.patch('/crime-records/2/', {'country': 'Canada'}, format='json')

        self.assertEqual(response.status_code, 200)

        self.assertEqual(
            response.content,
            '{"id":2,"country":"Canada","year":2015,"count":3,"rate":0.5,"source":"CTS","source_type":"CJ"}'
        )

    def test_delete_record(self):
        response = client.delete('/crime-records/2/', format='json')

        self.assertEqual(response.status_code, 204)

        response = client.get('/crime-records/', format='json')

        self.assertEqual(
            response.content,
            '{"count":1,"next":null,"previous":null,"results":[{"id":1,"country":"USA","year":2015,"count":2897,"rate":4.5,"source":"CTS","source_type":"CJ"}]}'
        )
