from django.contrib.auth.models import User

from rest_framework.serializers import ModelSerializer
from rest_framework.serializers import ModelSerializer

from crime_rates.models import CrimeRecords


class CrimeRecordsSerializer(ModelSerializer):
    class Meta:
        model = CrimeRecords

class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email')
