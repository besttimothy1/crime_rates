import csv

from django.core.management.base import BaseCommand

from crime_rates.models import CrimeRecords


class Command(BaseCommand):
    help = 'Imports UN crime data'

    def handle(self, *args, **options):
        self.import_csv_crime_data()

    def import_csv_crime_data(self):
        with open('crime_rates/fixtures/UNdata_Export_20160510_195918844.csv', 'rb') as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            reader.next()
            for row in reader:
                record, created = CrimeRecords.objects.get_or_create(
                    country=row[0],
                    year=row[1],
                    count=row[2],
                    rate=row[3],
                    source=row[4],
                    source_type=row[5]
                )
                if created:
                    print "created record for %s %s" % (record.country, record.year)
        return None
