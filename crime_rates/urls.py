from django.conf.urls import url, include

from rest_framework import routers

from crime_rates import views as crime_rates_views


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'crime-records', crime_rates_views.CrimeRecordsViewSet)
router.register(r'users', crime_rates_views.UserViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
