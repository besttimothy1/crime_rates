from django.contrib.auth.models import User

from rest_framework.filters import DjangoFilterBackend
from rest_framework.viewsets import ModelViewSet

from crime_rates.models import CrimeRecords
from crime_rates.serializers import CrimeRecordsSerializer
from crime_rates.serializers import UserSerializer


class CrimeRecordsViewSet(ModelViewSet):
    queryset = CrimeRecords.objects.all()
    serializer_class = CrimeRecordsSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('country', 'year',)


class UserViewSet(ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
